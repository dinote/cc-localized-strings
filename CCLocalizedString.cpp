
//
//  CCLocalizedString.c
//  tictactoedraw
//
//  Created by Dino Matijas on 1/3/13.
//
//

#include "CCLocalizedString.h"
#include "CCLocale.h"
#include "flexStringFileScanner.h"
#include "flexStringFileScanerExtras.h"
#include <vector>

using namespace cocos2d;
using namespace std;
CCStringsLoaderBase::CCStringsLoaderBase()
: nameBase("strings")
{
}

CCStringsLoaderBase::~CCStringsLoaderBase() {
}


const char* CCStringsLoaderBase::getStringsFilenameWithExtension(const char* extension) {
    
    
    CCString* defaultFilename = CCString::createWithFormat("%s.%s", nameBase.c_str(), extension);
    CCString* localeFilename = CCString::createWithFormat("%s-%s.%s", nameBase.c_str(), CCLocale::sharedLocale()->getLanguageCode(), extension);
    
    if (fileExists(localeFilename->getCString())) {
        return localeFilename->getCString();
    }
    
    return defaultFilename->getCString();
}

bool CCStringsLoaderBase::fileExists(const char* filename) {
    unsigned long size;
    CCFileUtils* sharedFileUtils = CCFileUtils::sharedFileUtils();
    bool isPopupNotify = sharedFileUtils->isPopupNotify();
    sharedFileUtils->setPopupNotify(false); //don't want popup when checking if file is available
    
    const char* path = sharedFileUtils->fullPathFromRelativePath(filename);
    unsigned char* data = sharedFileUtils->getFileData(path, "rb", &size);
    CC_SAFE_DELETE(data);
    
    sharedFileUtils->setPopupNotify(isPopupNotify);
    return size > 0;
}


CCPListStringsFileLoader::CCPListStringsFileLoader()
: strings(NULL)
{
    strings = CCDictionary::createWithContentsOfFile(this->getStringsFilenameWithExtension("plist"));
}
CCPListStringsFileLoader::~CCPListStringsFileLoader() {
    CC_SAFE_RELEASE(strings); strings = NULL;
}

const char* CCPListStringsFileLoader::getString(const std::string& key) {
    CCString* string = (CCString*)strings->objectForKey(key);
    if (!string) {
        return key.c_str();
    }
    return string->getCString();
}

CCPListStringsFileLoader* CCPListStringsFileLoader::sharedLoader() {
    static CCPListStringsFileLoader* instance = NULL;
    if (!instance) {
        instance = new CCPListStringsFileLoader();
    }
    return instance;
}

CCStringsFileLoader::CCStringsFileLoader()
: extension("localization")
{
    
}

CCStringsFileLoader::~CCStringsFileLoader() {
    
}

const char* CCStringsFileLoader::getString(const std::string& key) {
    bool keyExistst = strings.find(key) != strings.end();
    if (keyExistst) {
        return strings[key].c_str();
    }
    return key.c_str();
}

bool CCStringsFileLoader::init() {
    yyscan_t            scanner = NULL;
    YY_BUFFER_STATE     buf = NULL;
    vector<std::string> stringTokens;
    unsigned long       fileSize = 0;
    char*               fileContents = 0;
    FlexStringFileScannerExtra* extra = FlexStringFileScannerExtra::create();
    int tok;
    
    
    
    if (yylex_init_extra(extra, &scanner)) return false;
    const char* path = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath(getStringsFilenameWithExtension(extension.c_str()));
    fileContents = (char*)CCFileUtils::sharedFileUtils()->getFileData(path, "rb", &fileSize);
    if (!fileSize) {
        yylex_destroy(scanner);
        return false;
    }
    buf = yy_scan_bytes((char*)fileContents, fileSize, scanner);
    CC_SAFE_DELETE(fileContents);
    
    while ((tok=yylex(scanner)) > 0) {
        if (tok == kToken_STRING) {
            stringTokens.push_back(string(extra->getString()));
            extra->clearString();
        }
    }
    
    yy_delete_buffer(buf, scanner); buf = NULL;
    yylex_destroy(scanner); scanner = NULL;
    
    //stringTokens should be: key,value,key,value...
    CCAssert(stringTokens.size() % 2 == 0, "Error in strings file");
    for (int i = 0; i < stringTokens.size() / 2; i++) {
        strings[stringTokens[2*i]] = stringTokens[2*i + 1];
    }
    return true;
}


CCStringsFileLoader* CCStringsFileLoader::sharedLoader() {
    static CCStringsFileLoader* instance = NULL;
    if (!instance) {
        instance = new CCStringsFileLoader();
        instance->init();
    }
    return instance;
}



CCStringsPropertiesLoader::CCStringsPropertiesLoader() {
    nameBase = "app";
    extension = "properties";
}

CCStringsPropertiesLoader::~CCStringsPropertiesLoader() {
    
}

CCStringsPropertiesLoader* CCStringsPropertiesLoader::sharedLoader() {
    static CCStringsPropertiesLoader* instance = NULL;
    if (!instance) {
        instance = new CCStringsPropertiesLoader();
        instance->init();
    }
    return instance;
}

const char* CCStringsPropertiesLoader::getString(const std::string& key, const char* def) {
    bool keyExistst = strings.find(key) != strings.end();
    if (keyExistst) {
        return strings[key].c_str();
    }
    return def;
}

int CCStringsPropertiesLoader::getInt(const std::string& key, int def) {
    bool keyExistst = strings.find(key) != strings.end();
    if (keyExistst) {
        return atoi(strings[key].c_str());
    }
    return def;
}

