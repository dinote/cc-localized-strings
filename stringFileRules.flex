%option reentrant
%option outfile="flexStringFileScanner.cpp" header-file="flexStringFileScanner.h"
%option noyywrap
%option extra-type="FlexStringFileScannerExtra* "
%top {
    #include "flexStringFileScanerExtras.h"
    #include "cocos2d.h"    
    using namespace cocos2d;
}

%x STR
%x COMMENT

whitespace  [ \t\n\r]
%%
\"              { BEGIN(STR); yyextra->beginNewString(); }
<STR>\\t        { yyextra->addCharToString('\t'); }
<STR>\\n        { yyextra->addCharToString('\n'); }
<STR>\\\"       { yyextra->addCharToString('\"'); }
<STR>\"         { BEGIN(INITIAL); return kToken_STRING; }
<STR>.          { yyextra->addCharToString(*yytext); }

\/\*            { BEGIN(COMMENT); }
<COMMENT>\*\/   { BEGIN(INITIAL); }
<COMMENT>.      {} /* skip comments */

=               { return kToken_EQUALS; }
.               {} /* ignore everything else */

%%