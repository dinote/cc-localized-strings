//
//  CCLocale.cpp
//  tictactoedraw
//
//  Created by Dino Matijas on 1/3/13.
//
//

#include "CCLocale.h"

const char* CCLocale::getLanguageCode() {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    // get the current language code.(such as English is "en", Chinese is "zh" and so on)
    NSDictionary* temp = [NSLocale componentsFromLocaleIdentifier:currentLanguage];
    NSString * languageCode = [temp objectForKey:NSLocaleLanguageCode];
    return [languageCode UTF8String];
}

CCLocale* CCLocale::sharedLocale() {
    CCLocale* instance = NULL;
    if (!instance) {
        instance = new CCLocale();
    }
    return instance;
}
