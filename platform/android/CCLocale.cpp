//
//  CCLocale.cpp
//  tictactoedraw
//
//  Created by Dino Matijas on 1/3/13.
//
//

#include "CCLocale.h"
#include "jni/JniHelper.h"
#include "jni/Java_org_cocos2dx_lib_Cocos2dxHelper.h"
#include <jni.h>

const char* CCLocale::getLanguageCode() {
    return getCurrentLanguageJNI();
}

CCLocale* CCLocale::sharedLocale() {
    CCLocale* instance = NULL;
    if (!instance) {
        instance = new CCLocale();
    }
    return instance;
}

