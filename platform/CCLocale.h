//
//  CCLocale.h
//  tictactoedraw
//
//  Created by Dino Matijas on 1/3/13.
//
//

#ifndef __tictactoedraw__CCLocale__
#define __tictactoedraw__CCLocale__

class CCLocale {
public:
    virtual const char* getLanguageCode();    
    static CCLocale* sharedLocale();
};

#endif /* defined(__tictactoedraw__CCLocale__) */
