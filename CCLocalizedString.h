//
//  CCLocalizedString.h
//  tictactoedraw
//
//  Created by Dino Matijas on 1/3/13.
//
//

#ifndef __tictactoedraw__CCLocalizedString__
#define __tictactoedraw__CCLocalizedString__

#include <string>
#include <map>
#include "cocos2d.h"

#define CC_LOCALIZED_PLIST(key, desc) CCLocalizedString<CCPListStringsFileLoader>(key, desc)
#define CC_LOCALIZED_STRINGS(key, desc) CCLocalizedString<CCStringsFileLoader>(key, desc)

#define CC_LOCALIZED(key) CCLocalizedString<CCStringsFileLoader>(key)

#define CC_APP_PROPERTY(key, def) CCStringsPropertiesLoader::sharedLoader()->getString(key, def)

template<typename loader>
const char* CCLocalizedString(const std::string& key, const char* description) {
    CC_UNUSED_PARAM(description);
    return loader::sharedLoader()->getString(key);
}

template<typename loader>
const char* CCLocalizedString(const std::string& key) {    
    return CCLocalizedString<loader>(key, NULL);
}


class CCStringsLoaderBase {
public:
    CCStringsLoaderBase();
    virtual ~CCStringsLoaderBase();
    virtual const char* getString(const std::string& key) = 0;
    
protected:
    std::string nameBase;
    virtual bool fileExists(const char* filename);
    /*
     @brief based on locale return the filename that has the strings
     @returns filename (without extension) that has the strings
     */
    virtual const char* getStringsFilenameWithExtension(const char* extension);
};


class CCPListStringsFileLoader : public CCStringsLoaderBase {
public:
    CCPListStringsFileLoader();
    virtual ~CCPListStringsFileLoader();
    
    virtual const char* getString(const std::string& key);
    static CCPListStringsFileLoader* sharedLoader();
    
protected:
    cocos2d::CCDictionary*      strings;
};

class CCStringsFileLoader : public CCStringsLoaderBase {
public:
    CCStringsFileLoader();
    virtual ~CCStringsFileLoader();
    
    virtual const char* getString(const std::string& key);
    static CCStringsFileLoader* sharedLoader();
    
protected:
    std::string     extension;
    virtual bool init();
    std::map<std::string, std::string>  strings;
};


class CCStringsPropertiesLoader : public CCStringsFileLoader {
public:
    CCStringsPropertiesLoader();
    virtual ~CCStringsPropertiesLoader();
    static CCStringsPropertiesLoader* sharedLoader();
    
    virtual const char* getString(const std::string& key, const char* def);
    virtual int getInt(const std::string& key, int def);
};


#endif /* defined(__tictactoedraw__CCLocalizedString__) */
