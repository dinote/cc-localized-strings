//
//  flexStringFileScanerTokens.h
//  tictactoedraw
//
//  Created by Dino Matijas on 1/3/13.
//
//

#ifndef tictactoedraw_flexStringFileScanerTokens_h
#define tictactoedraw_flexStringFileScanerTokens_h

#include "cocos2d.h"
#include <sstream>
#include <string>
typedef enum tagLexToken {
    kToken_EQUALS = 1,
    kToken_STRING
} LexTokenEnum;

class FlexStringFileScannerExtra : cocos2d::CCObject {
public:
    void beginNewString() { clearString(); }
    void addCharToString(const char theChar) { this->ss << theChar; }
    void clearString() { this->ss.str(""); this->ss.clear(); }
    std::string getString() { return this->ss.str(); }
    
    static FlexStringFileScannerExtra* create() { FlexStringFileScannerExtra* a = new FlexStringFileScannerExtra(); a->autorelease(); return a; }
protected:
    std::stringstream ss;
};

#endif
